package CustomStepDefenition;

import static org.junit.Assert.assertEquals;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import io.restassured.specification.RequestSpecification;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.hamcrest.Matcher;
import org.hamcrest.SelfDescribing;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.runners.Parameterized.Parameter;

import RESTAssured_CucumBDD.MainClass;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import io.cucumber.java.After;
import io.cucumber.java.en.And;
//import cucumber.api.junit.Cucumber;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.http.Header;
import io.restassured.path.xml.XmlPath;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;
import javafx.util.Pair;

public class Stepdefinition {

	MainClass Rest = new MainClass();
	public static RequestSpecification spec;
	public static Header h;
	public static Parameter p;

//    @RunWith(Cucumber.class)
//    public class MyStepDefinitions {

	public static String Path;
	public static Response GetResponse;
	public static ValidatableResponse Response;
	public static Scenario s;
	static JSONObject RequestParameters = new JSONObject();
	public static String Temp_Variable;
	 

	
	@After()
	public void after_scenario() {
		System.out.println("\nClose http://localhost:3030/products\n");
	}
	
	@Before("@First")
	public void before_scenario1() {
		System.out.println("\n+++++++++++++++++++Initiate http://localhost:3030/products\n");
	}
	
	@After("@First")
	public void after_scenario1() {
		System.out.println("\n+++++++++++++++++Close http://localhost:3030/products\n");
	}

	@Before()
	    public void before_scenario(Scenario scenario) {
	        this.s = scenario;
	    //    System.out.println("\nInitiate http://localhost:3030/products\n");
	    }
	@Given("URL {string}")
	public String URL(String point) {

		Path = point;
		return Path;

	}

	@When("Method {string}")

	public void Method(String Method_Name) {

		switch (Method_Name) {
		case "Get":
			GetResponse = Rest.Get(Path);
			break;

		case "Delete":
			GetResponse = Rest.Delete(Path);
			break;
		case "Post":
			GetResponse = Rest.Post(Path, RequestParameters);
		}
	}

	@Then("Statuscode {int}")
	public void statuscode(int Expected) {

		int Actual = GetResponse.andReturn().statusCode();
		assertEquals("Status code Validation", Expected,Actual);
	}

	@Then("Match JSONPath {string} contains {string}")
	public void match_JSONPath_contains(String path, String Expected) {

		String Actual = GetResponse.getBody().jsonPath().get(path).toString();
		assertEquals(" Json Path Value Check ", Expected,Actual);
	}

	@Then("Match header {string} contains {string}")
	public void match_header_contains(String string, String Expected) {

		String Actual = GetResponse.getHeader(string);
		if (Objects.equals(Actual, Expected))
			assertEquals("Header Matching",Expected , Actual);

	}

	@Then("Print responsetime")
	public void print_responsetime() {
		long temp = GetResponse.getTime();
		System.out.println("Response Time" + temp + "ms");
	}

	@Then("Print response")
	public void print_response() {
		System.out.println("Response:" + GetResponse.getBody().prettyPrint());
	}

	@When("Method {string} with file {string}")
	public void method_with_file(String Method, File Filepath) {

	}

	@Given("URL {string} and file {string} When Method {string}")
	public void url_and_file_When_Method(String url, String file, String Method) throws Throwable {
		switch (Method) {
		case "Post":
			GetResponse = Rest.Post(url, file);
			break;

		case "Put":
			GetResponse = Rest.Put(url, file);
			break;

		case "SOAP":
			GetResponse = Rest.SOAP(url, file);
			break;
		}

	}

	@Then("Print response to file {string}")
	public void print_response_to_file(String path) throws ClassNotFoundException, IOException, ParseException {
		String Response = GetResponse.getBody().prettyPrint();
		File f = new File(path);
		boolean Exists = f.exists();
		if (Exists) {
			System.out.println("Printing response to File");
			this.write(f, Response);
		} else {
			f.createNewFile();
			System.out.println("New file created");
			this.write(f, Response);
		}

	}

	private void write(File f, String Response) {
		try {
			FileOutputStream f1 = new FileOutputStream(f);
			ObjectOutputStream o = new ObjectOutputStream(f1);
			o.writeObject(Response);
			o.close();
			f1.close();
		} catch (FileNotFoundException e) {
			System.out.println("File not found");
		} catch (IOException e) {
			System.out.println("Error initializing stream");
		}
	}

	@And("Match XMLPath {string} contains {string}")
	public void XMLPath(String xmlpath, String Expected) {

		XmlPath xml = new XmlPath(GetResponse.asString());
		String Actual = xml.getString(xmlpath);
		assertEquals("Xml Path String check", Actual, Expected);

	}

	@SuppressWarnings("unchecked")
	@Given("parameter JSON {string} with value {string}")
	public JSONObject parameter_JSON_with_value(String key, String value) {
		RequestParameters.put(key, value);
		System.out.println("\n" + RequestParameters);
		return RequestParameters;
	}

	@SuppressWarnings("unchecked")
	@Given("parameter JSON {string} with value {double}")
	public JSONObject parameter_with_value(String key, Double value) {
		RequestParameters.put(key, value);
		System.out.println("\n" + RequestParameters.toJSONString());
		return RequestParameters;
	}

	@When("Method with params {string}")
	public void method_with_params(String string) {
		GetResponse = Rest.Post(Path, RequestParameters);
	}

	@When("Print report")
	public void print_report() {

		s.write(GetResponse.getBody().prettyPrint());

	}
	@When("Method {string} appended with Stored Value")
	public void Method_Appended(String Method_Name) {
		
		System.out.println("\nMethod name with appended value: " + Method_Name);
		switch (Method_Name) {
		case "Get":
			GetResponse = Rest.Get(Path+"/"+Temp_Variable);
			break;
		case "Post":
			GetResponse = Rest.Post(Path+"/"+Temp_Variable);
			break;
		case "Put":
			GetResponse = Rest.Put(Path+"/"+Temp_Variable);
			break;
		case "Delete":
			GetResponse = Rest.Delete(Path+"/"+Temp_Variable);
			break;
		}
	}
	@And("get value of {string} and store in {string}")
	public String getting_recent_value(String JSON_Path, String Local_Temp_Variable) {
		
		Local_Temp_Variable = GetResponse.getBody().jsonPath().getJsonObject(JSON_Path).toString();
		Temp_Variable = Local_Temp_Variable;
		return Temp_Variable;
	}
	@Given("Query Param name {string} value {string}")
	public void param_name_value(String string, String string2) {
	    spec = given().queryParam(string, string2);
	}
	@When("Method {string} with param")
	public void method_with_param(String string) {
		switch (string) {
		case "Get":
			GetResponse = Rest.Get(Path,spec);
			break;
		case "Post":
			GetResponse = Rest.Post(Path,spec);
			break;
		case "Put":
			GetResponse = Rest.Put(Path,spec);
			break;
		case "Delete":
			GetResponse = Rest.Delete(Path,spec);
			break;
		}
	}



}
