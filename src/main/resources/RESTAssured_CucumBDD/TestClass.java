package RESTAssured_CucumBDD;

import static io.restassured.RestAssured.given;

import io.restassured.specification.RequestSpecification;

public class TestClass {
	static String[] key = new String[20];
	static String[] value = new String[20];

	public static void main(String[] args) {
		method("1", "1", "2", "2");

	}

	public static void method(String... strings) {

		int n = strings.length;
		System.out.println(n);
		int i = 0;
		int j = 0;
		while (i < n) {
			if (i % 2 == 0) {
				System.out.println("if");
				key[j] = strings[i];
				i++;
			}

			if (i % 2 != 0) {
				System.out.println("else");
				value[j] = strings[i];
				j++;
				i++;
			}
		}

		for (i = 0; i < n / 2; i++) {
			System.out.println("key" + key[i]);
			System.out.println("Value" + value[i]);
		}

	}

	public RequestSpecification getheaderinfo(String key, String value) {
		RequestSpecification spec = given().when().headers(key, value);
		return spec;
	}
}
