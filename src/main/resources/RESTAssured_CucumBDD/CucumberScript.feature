Feature: Testing HTTP methods using cucumber BDD 
	
Scenario: Get Method
 Given URL 'http://localhost:3030/products/9999766'
 When Method 'Get'
 Then Statuscode 200 
 And Match JSONPath 'id' contains '9999766'
 And Match header 'Content-type' contains 'application/json; charset=utf-8'  
 #Content-Type: application/json; charset=utf-8,Content-Length: 255,ETag: W/"ff-s04YLPwIwsg20xMxRcAaWZZ/lGM,Date: Wed, 14 Aug 2019 09:44:29 GMT,Connection: keep-alive
 And Print responsetime
 And Print response
 And Print response to file 'C:\Users\user\Downloads\Output files\reqGET.json'
 
 Scenario: passing 1 param
	Given URL 'https://reqres.in/api/users' 
	And Query Param name 'page' value '2' 
	When Method 'Get' with param
	Then Statuscode 200 
	And Print response 
	And Print response to file 'C:\Users\user\Downloads\Output files\reqGET1.txt' 
	
Scenario: post Method - with file
Given URL 'http://localhost:3030/products' and file 'C:\\Users\\user\\Documents\\Eclipse Workspace\\RestAssured\\src\\test\\java\\com\\rest\\RestAssured\\req.json' When Method 'Post'
Then Statuscode 201
And Match JSONPath 'name' contains 'New Product' 
And Match header 'Content-Length' contains '336'
And Print responsetime
And Print response
                    
					
Scenario: Put Method - with file
Given URL 'http://localhost:3030/products/9999777' and file "C:\\Users\\user\\Documents\\Eclipse Workspace\\RestAssured\\src\\test\\java\\com\\rest\\RestAssured\\req.json" When Method 'Put'
Then Statuscode 200
And Match JSONPath 'name' contains 'New Product' 
And Match header 'Content-Length' contains '315'
And Print responsetime
And Print response
 
Scenario: Delete
Given URL 'http://localhost:3030/products/9999775'
 When Method 'Delete'
 Then Statuscode 200 
 And Print responsetime
 And Print response
 When Method "Get"
 And Print response
 
 Scenario: Adding params dynamically
 Given URL "http://localhost:3030/products"
    And parameter JSON "name" with value "Jii"
    And parameter JSON "type" with value "Hard Jii Goods"
    And parameter JSON "upc" with value "9898787849"
    And parameter JSON "price" with value 44.66
    And parameter JSON "description" with value "This is a new something products."
    And parameter JSON "model" with value "NP1224455"
    When Method "Post"
    And Print response
	And Print report
	
Scenario: SOAP
Given URL 'http://www.dneonline.com/calculator.asmx' and file 'C:\\Users\\user\\Documents\\Eclipse Workspace\\RESTAssured_CucumberBDD\\src\\main\\java\\com\\restassured\\RESTAssured_CucumberBDD\\SOAP.txt' When Method 'SOAP'
Then Statuscode 200
And Match XMLPath "AddResult" contains "20"
And Print responsetime
And Print response

 Scenario: Request Chaining
 Given URL "http://localhost:3030/products"
    And parameter JSON "name" with value "Jii"
    And parameter JSON "type" with value "Hard Jii Goods"
    And parameter JSON "upc" with value "9898787849"
    And parameter JSON "price" with value 44.66
    And parameter JSON "description" with value "This is a new something products."
    And parameter JSON "model" with value "NP1224455"
    When Method "Post"
    And Print response
    And get value of "id" and store in "Stored Value"
	When Method "Get" appended with Stored Value
	And Print report
