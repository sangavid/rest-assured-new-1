$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:/C:/Users/user/Documents/Eclipse%20Workspace/RESTAssured_CucumberBDD/src/main/resources/RESTAssured_CucumBDD/CucumberScript.feature");
formatter.feature({
  "name": "Testing HTTP methods using cucumber BDD",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "Get Method",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "URL \u0027https://reqres.in/api/users?page\u003d2\u0027",
  "keyword": "Given "
});
formatter.match({
  "location": "Stepdefinition.URL(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Method \u0027Get\u0027",
  "keyword": "When "
});
formatter.match({
  "location": "Stepdefinition.Method(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Statuscode \u0027200\u0027",
  "keyword": "Then "
});
formatter.match({
  "location": "Stepdefinition.statuscode(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Match JSONPath \u0027total\u0027 contains \u002712\u0027",
  "keyword": "And "
});
formatter.match({
  "location": "Stepdefinition.match_JSONPath_contains(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "print response",
  "keyword": "And "
});
formatter.match({
  "location": "Stepdefinition.print_response()"
});
formatter.result({
  "status": "passed"
});
});