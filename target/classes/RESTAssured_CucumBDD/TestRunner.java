package RESTAssured_CucumBDD;

import java.io.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.junit.runner.RunWith;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import net.masterthought.cucumber.Configuration;
import net.masterthought.cucumber.ReportBuilder;
import net.masterthought.cucumber.Reportable;
import net.masterthought.cucumber.presentation.PresentationMode;

@RunWith(Cucumber.class)
@CucumberOptions(
	features = ".\\src\\main\\resources\\RESTAssured_CucumBDD\\CucumberScript.feature",
	glue= {"CustomStepDefenition"},
	 plugin = { "pretty", "json:Report/cucumber.json" 
			 },
			 monochrome = true
			)
public class TestRunner {
	
	@AfterClass
    public static void writeReport(){
		String Path= System.getProperty("user.dir")+"\\Report";   
                 
         Collection<File> jsonFiles = FileUtils.listFiles(new File(Path), new String[] {"json"}, true);
         List<String> jsonPaths =new ArrayList<String>(jsonFiles.size());
         //jsonPaths.add(Path);
         jsonFiles.forEach(file -> jsonPaths.add(file.getAbsolutePath()));
         Configuration config = new Configuration(new File("Report"),"demo");
         ReportBuilder reportBuilder = new ReportBuilder(jsonPaths, config);
         reportBuilder.generateReports();  
    }
	
}
